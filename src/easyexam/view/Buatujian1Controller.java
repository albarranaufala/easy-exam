/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class Buatujian1Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    private EasyExam easyExam;
    private Stage buatUjian1Stage;
    private boolean okDitekan = false;
    private Ujian ujian;

    @FXML
    private TextField tfJudulUjian;
    @FXML
    private TextField tfJumlahSoal;
    @FXML
    private TextField tfKetuntasan;
    @FXML
    private TextField tfKodeUjian;
    private boolean udahDibuat;

    public void setUdahDibuat(boolean udahDibuat) {
        this.udahDibuat = udahDibuat;
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;
    }

    public void setStage(Stage buatUjian1Stage) {
        this.buatUjian1Stage = buatUjian1Stage;
    }

    public void setUjian(Ujian ujian) {
        this.ujian = ujian;

        tfJudulUjian.setText(ujian.getJudulUjian());
        tfJumlahSoal.setText(Integer.toString(ujian.getJumlahSoal()));
        tfKetuntasan.setText(Integer.toString(ujian.getNilaiKetuntasan()));
        tfKodeUjian.setText(ujian.getKodeUjian());
    }

    @FXML
    public void handleTombolBuatSoal() {
        if (isInputValid()) {
            ujian.setJudulUjian(tfJudulUjian.getText());
            ujian.setJumlahSoal(Integer.parseInt(tfJumlahSoal.getText()));
            ujian.jumlahSoalProperty().set(tfJumlahSoal.getText());
            ujian.setNilaiKetuntasan(Integer.parseInt(tfKetuntasan.getText()));
            ujian.ketuntasanProperty().set(tfKetuntasan.getText());
            ujian.setKodeUjian(tfKodeUjian.getText());

            okDitekan = true;
            boolean okDitekan1 = easyExam.showBuatUjian2(ujian);
            if (okDitekan1) {
                this.buatUjian1Stage.close();
            }
        }
    }

    @FXML
    public void handleTombolBatal() {
        buatUjian1Stage.close();
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (tfJudulUjian.getText() == null || tfJudulUjian.getText().length() == 0) {
            errorMessage += "Judul ujian tidak valid!\n";
        }
        if (tfJumlahSoal.getText() == null || tfJumlahSoal.getText().length() == 0) {
            errorMessage += "Jumlah soal tidak valid!\n";
        } else {
            // try untuk integer di jumlah soal
            try {
                Integer.parseInt(tfJumlahSoal.getText());
                if(Integer.parseInt(tfJumlahSoal.getText())<=0){
                    errorMessage += "Jumlah soal harus diatas 0!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Jumlah soal tidak valid (harus angka)!\n";
            }
        }
        if (tfKetuntasan.getText() == null || tfKetuntasan.getText().length() == 0) {
            errorMessage += "Durasi tidak valid!\n";
        } else {
            // try untuk integer di nilai ketuntasan
            
            try {
                Integer.parseInt(tfKetuntasan.getText());
                if(Integer.parseInt(tfKetuntasan.getText())<=0){
                    errorMessage += "Durasi harus diatas 0!\n";
                }
            } catch (NumberFormatException e) {
                errorMessage += "Durasi tidak valid (harus angka)!\n";
            }
        }
        if (tfKodeUjian.getText() == null || tfKodeUjian.getText().length() == 0) {
            errorMessage += "Kode ujian tidak valid!\n";
        }
        boolean sudahPernah = false;
        for (int i = 0; i < easyExam.getDataUjian().size(); i++) {
            if (easyExam.getDataUjian().get(i).getKodeUjian().equals(tfKodeUjian.getText())) {
                sudahPernah = true;
            }
        }

        if (sudahPernah) {
            errorMessage += "Kode ujian sudah dipakai!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // nampilin pesan error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(buatUjian1Stage);
            alert.setTitle("Kesalahan Mengisi");
            alert.setHeaderText("Silahkan isi form dengan valid");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    public boolean isokDitekan() {
        return okDitekan;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
