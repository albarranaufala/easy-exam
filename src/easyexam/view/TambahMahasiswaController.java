/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class TambahMahasiswaController implements Initializable {

    private Stage stage;
    private EasyExam easyExam;
    private ObservableList<Mahasiswa> dataMahasiswa = FXCollections.observableArrayList();
    private boolean okDitekan = false;
    private Mahasiswa mahasiswa;

    @FXML
    private TextField tfNama;
    @FXML
    private TextField tfNim;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;
    }

    public void setDataMahasiswa(ObservableList<Mahasiswa> dataMahasiswa) {
        this.dataMahasiswa = dataMahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa = mahasiswa;
        
        tfNama.setText(mahasiswa.getNama());
        tfNim.setText(mahasiswa.getNIM());
    }
    
    public boolean isOkDitekan() {
        return this.okDitekan;
    }

    @FXML
    public void handleTambahMahasiswa() {
        if (isInputValid()) {
            mahasiswa.setNama(tfNama.getText());
            mahasiswa.setNIM(tfNim.getText());
            mahasiswa.setPassword(tfNim.getText());
            mahasiswa.encryptPassword();
            okDitekan = true;
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(stage);
            alert.setTitle("Menambah Mahasiswa Berhasil");
            alert.setHeaderText("Menambah Mahasiswa Baru Berhasil");
            alert.setContentText("NIM diatur sebagai password secara default.");

            alert.showAndWait();
            
            stage.close();
        }
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (tfNama.getText() == null || tfNama.getText().length() == 0) {
            errorMessage += "Nama tidak valid!\n";
        }
        if (tfNim.getText() == null || tfNim.getText().length() == 0) {
            errorMessage += "Nim tidak valid!\n";
        } else {
            boolean telahAda = false;
            for (int i = 0; i < dataMahasiswa.size(); i++) {
                if (dataMahasiswa.get(i).getNIM().equals(tfNim.getText())) {
                    telahAda = true;
                }
            }
            if(tfNim.getText().equals(mahasiswa.getNIM())){
                telahAda=false;
            }
            if (telahAda) {
                errorMessage += "NIM telah ada yang punya!\n";
            }
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // nampilin pesan error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(stage);
            alert.setTitle("Kesalahan Mengisi");
            alert.setHeaderText("Silahkan isi form dengan valid");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    @FXML
    public void handleTombolBatal() {
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
