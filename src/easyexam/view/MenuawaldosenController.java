/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Dosen;
import easyexam.model.Mahasiswa;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Alert;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class MenuawaldosenController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TableView<Ujian> tabelUjian;
    @FXML
    private TableColumn<Ujian, String> kolomJudulUjian;
    @FXML
    private TableColumn<Ujian, String> kolomKodeSoal;
    @FXML
    private TableColumn<Ujian, String> kolomJumlah;
    @FXML
    private TableColumn<Ujian, String> kolomDurasi;

    ObservableList<PieChart.Data> dataPC = FXCollections.observableArrayList();
    @FXML
    private PieChart pcUjian;
    @FXML
    private Menu profil;

    private EasyExam easyExam;
    private Dosen dosen;

    private Stage menuAwalStage;

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
        profil.setText(dosen.getNama());
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;

        tabelUjian.setItems(easyExam.getDataUjian());

    }

    public void setMenuAwalStage(Stage menuAwalStage) {
        this.menuAwalStage = menuAwalStage;
    }

    @FXML
    public void handleTombolBuatSoal(ActionEvent event) {
        Ujian ujianSementara = new Ujian();
        boolean okDitekan = easyExam.showBuatUjian1(ujianSementara);
        if (okDitekan) {
            easyExam.getDataUjian().add(ujianSementara);
            easyExam.saveDataTabel();
        }
    }

    @FXML
    public void handleAboutUs() {
        easyExam.showAboutUs();
    }

    @FXML
    public void onEditJudulSoal(CellEditEvent editedCell) {
        Ujian ujian = tabelUjian.getSelectionModel().getSelectedItem();
        ujian.setJudulUjian(editedCell.getNewValue().toString());
        easyExam.saveDataTabel();
    }

    @FXML
    public void onEditJumlah(CellEditEvent editedCell) {
        Ujian ujian = tabelUjian.getSelectionModel().getSelectedItem();
        int jumlahTmp = ujian.getJumlahSoal();
        try {
            ujian.setJumlahSoal(Integer.parseInt(editedCell.getNewValue().toString()));
            easyExam.saveDataTabel();
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Jumlah Ujian Tidak Valid");
            alert.setHeaderText("Jumlah Ujian Tidak Valid");
            alert.setContentText("Silahkan isi jumlah ujian dengan bilangan.");

            alert.showAndWait();
            ujian.setJumlahSoal(jumlahTmp);
        }

    }

    @FXML
    public void onEditDurasi(CellEditEvent editedCell) {
        Ujian ujian = tabelUjian.getSelectionModel().getSelectedItem();
        int durasiTmp = ujian.getNilaiKetuntasan();
        try {
            ujian.setNilaiKetuntasan(Integer.parseInt(editedCell.getNewValue().toString()));
            easyExam.saveDataTabel();
        } catch (NumberFormatException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Durasi Ujian Tidak Valid");
            alert.setHeaderText("Durasi Ujian Tidak Valid");
            alert.setContentText("Silahkan isi durasi ujian dengan bilangan.");

            alert.showAndWait();
            ujian.setNilaiKetuntasan(durasiTmp);
        }
    }


    @FXML
    public void handleTombolDetail(ActionEvent event) {
        Ujian ujianTerpilih = tabelUjian.getSelectionModel().getSelectedItem();

        if (ujianTerpilih != null) {
            easyExam.showDetailMahasiswaDetail(ujianTerpilih);
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Tidak ada pilihan");
            alert.setHeaderText("Anda tidak memilih");
            alert.setContentText("Silahkan pilih yang ingin anda lihat.");

            alert.showAndWait();
        }
    }

    @FXML
    private void handleTombolHapus() {
        int selectedIndex = tabelUjian.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            tabelUjian.getItems().remove(selectedIndex);
            //simpan perubahan
            easyExam.saveDataTabel();
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Tidak ada pilihan");
            alert.setHeaderText("Anda tidak memilih");
            alert.setContentText("Silahkan pilih yang mau anda hapus.");

            alert.showAndWait();
        }
    }

    @FXML
    private void handleTombolEdit() {
        Ujian ujianTerpilih = tabelUjian.getSelectionModel().getSelectedItem();
        if (ujianTerpilih != null) {
            boolean okClicked = easyExam.showBuatUjian2(ujianTerpilih);
            if (okClicked) {
                easyExam.saveDataTabel();
                showDetailUjianPC(ujianTerpilih);
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Pilihan Tidak Ada");
            alert.setHeaderText("Tidak Ada Ujian Yang Terpilih");
            alert.setContentText("Silahkan pilih ujian yang mau diedit.");

            alert.showAndWait();
        }
    }

    @FXML
    public void handleTombolLogout(ActionEvent event) {
        try {
            menuAwalStage.close();
            easyExam.showMenuLogin();
        } catch (Exception e) {

        }

    }

    private void showDetailUjianPC(Ujian ujian) {
        try {
            if (ujian != null) {
                dataPC.removeAll(dataPC);
                dataPC.add(new PieChart.Data("A", ujian.getJumlahA()));
                dataPC.add(new PieChart.Data("A-", ujian.getJumlahAmin()));
                dataPC.add(new PieChart.Data("A/B", ujian.getJumlahAperB()));
                dataPC.add(new PieChart.Data("B+", ujian.getJumlahBplus()));
                dataPC.add(new PieChart.Data("B", ujian.getJumlahB()));
                dataPC.add(new PieChart.Data("B-", ujian.getJumlahBmin()));
                dataPC.add(new PieChart.Data("B/C", ujian.getJumlahBperC()));
                dataPC.add(new PieChart.Data("C+", ujian.getJumlahCplus()));
                dataPC.add(new PieChart.Data("C", ujian.getJumlahC()));
                dataPC.add(new PieChart.Data("C-", ujian.getJumlahCmin()));
                dataPC.add(new PieChart.Data("C/D", ujian.getJumlahCperD()));
                dataPC.add(new PieChart.Data("D+", ujian.getJumlahDplus()));
                dataPC.add(new PieChart.Data("D", ujian.getJumlahD()));
                dataPC.add(new PieChart.Data("E", ujian.getJumlahE()));
                pcUjian.setData(dataPC);
            } else {
                dataPC.removeAll(dataPC);
                pcUjian.setData(null);
            }
        } catch (Exception e) {

        }

    }

    @FXML
    public void handleTombolTambahMahasiswa(ActionEvent event) {
        Mahasiswa tmp = new Mahasiswa();
        boolean okDitekan = easyExam.showTambahMahasiswa(tmp);
        if (okDitekan) {
            //simpan
            easyExam.getDataMahasiswa().add(tmp);
            easyExam.saveDataMahasiswa();
        }
    }

    @FXML
    public void handleTombolDataMahasiswa(ActionEvent event) {
        easyExam.showDataMahasiswa();
    }

    @FXML
    public void handleEditData(ActionEvent event) {
        boolean okDitekan = easyExam.showEditData(dosen);
        if (okDitekan) {
            easyExam.saveListDosen();
            profil.setText(dosen.getNama());
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        //Inisialisasi tabel ujian dengan 2 kolom
        kolomJudulUjian.setCellValueFactory(cellData -> cellData.getValue().judulSoalProperty());
        kolomKodeSoal.setCellValueFactory(cellData -> cellData.getValue().kodeSoalProperty());
        kolomJumlah.setCellValueFactory(cellData -> cellData.getValue().jumlahSoalProperty());
        kolomDurasi.setCellValueFactory(cellData -> cellData.getValue().ketuntasanProperty());

        showDetailUjianPC(null);

        tabelUjian.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetailUjianPC(newValue));

        tabelUjian.setEditable(true);
        kolomJudulUjian.setCellFactory(TextFieldTableCell.forTableColumn());
        kolomJumlah.setCellFactory(TextFieldTableCell.forTableColumn());
        kolomDurasi.setCellFactory(TextFieldTableCell.forTableColumn());
    }

}
