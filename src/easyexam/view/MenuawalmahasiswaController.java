/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import easyexam.model.UjianSelesai;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.Menu;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class MenuawalmahasiswaController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private Stage menuAwalStage;
    private EasyExam easyExam;
    private Mahasiswa mahasiswa;

    @FXML
    private TableView<UjianSelesai> tabelUjianTerselesaikan;
    @FXML
    private TableColumn<UjianSelesai, String> kolomJudulUjian;
    @FXML
    private TableColumn<UjianSelesai, String> kolomNilai;

    @FXML
    private TextField tfKodeSoal;
    @FXML
    private Menu profil;
    @FXML
    private Label lbJudulSoal;
    @FXML
    private Label lbJumlahSoal;
    @FXML
    private Label lbJawabanBenar;
    @FXML
    private Label lbJawabanSalah;
    @FXML
    private Label lbNilai;

    public void setMenuAwalStage(Stage menuAwalStage) {
        this.menuAwalStage = menuAwalStage;
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;

    }

    
    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
        tabelUjianTerselesaikan.setItems(mahasiswa.getUjianUjianSelesaiObservable());
    }

    public void setTulisanNama() {
        profil.setText(mahasiswa.getNama());
    }

    @FXML
    public void handleTombolLogout(ActionEvent event) {
        menuAwalStage.close();
        easyExam.showMenuLogin();
    }

    @FXML
    public void handleTombolMulaiUjian(ActionEvent event) {
        String inputKodeSoal = tfKodeSoal.getText();
        boolean ditemukan = false;
        int indeksSoal = 0;
        for (int i = 0; i < easyExam.getDataUjian().size(); i++) {
            if (easyExam.getDataUjian().get(i).getKodeUjian().equals(tfKodeSoal.getText())) {
                ditemukan = true;
                indeksSoal = i;
            }
        }
        boolean belumDikerjakan = true;
        for(int i = 0; i < mahasiswa.getUjianUjianSelesai().size(); i++){
            if(mahasiswa.getUjianUjianSelesai().get(i).getKodeUjian().equals(tfKodeSoal.getText())){
                belumDikerjakan = false;
            }
        }
        if (ditemukan && belumDikerjakan) {
            boolean okDitekan = easyExam.showMenuMulaiUjian1(easyExam.getDataUjian().get(indeksSoal), mahasiswa);
            if (okDitekan) {
                //Simpan Perubahan
                
                easyExam.saveDataMahasiswa();
                easyExam.saveDataTabel();
            }
        } else {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Kode Soal Salah");
            alert.setHeaderText("Kode Soal Tidak Ditemukan atau Sudah Pernah Dikerjakan");
            alert.setContentText("Silahkan masukkan kode soal dengan benar atau yang belum pernah dikerjakan!");

            alert.showAndWait();
        }

    }
    
    @FXML
    public void handleAboutUs(){
        easyExam.showAboutUs();
    }

//    @FXML
//    public void handleTombolHapus(ActionEvent event) {
//        int selectedIndex = tabelUjianTerselesaikan.getSelectionModel().getSelectedIndex();
//        if (selectedIndex >= 0) {
//            tabelUjianTerselesaikan.getItems().remove(selectedIndex);
//            //simpan perubahan;
//            easyExam.saveDataMahasiswa();
//            easyExam.saveDataTabel();
//        } else {
//            // Nothing selected.
//            Alert alert = new Alert(Alert.AlertType.WARNING);
//            alert.initOwner(easyExam.getPrimaryStage());
//            alert.setTitle("Tidak ada pilihan");
//            alert.setHeaderText("Anda tidak memilih");
//            alert.setContentText("Silahkan pilih yang mau anda hapus.");
//
//            alert.showAndWait();
//        }
//    }

    private void showDetailUjianSelesai(UjianSelesai ujianSelesai) {
        if (ujianSelesai != null) {
            // Fill the labels with info from the ujianSelesai object.
            lbJudulSoal.setText(ujianSelesai.getJudulUjian());
            lbJumlahSoal.setText(Integer.toString(ujianSelesai.getJumlahSoal()));
            lbJawabanBenar.setText(Integer.toString(ujianSelesai.getJawabanBenar()));
            lbJawabanSalah.setText(Integer.toString(ujianSelesai.getJawabanSalah()));
            lbNilai.setText(Integer.toString(ujianSelesai.hitungNilai()));
        } else {
            // jika Ujian null, kosongkan text
            lbJudulSoal.setText("");
            lbJumlahSoal.setText("");
            lbJawabanBenar.setText("");
            lbJawabanSalah.setText("");
            lbNilai.setText("");
        }
    }

    @FXML
    public void handleTombolDetail(ActionEvent event) {
        UjianSelesai ujianTerpilih = tabelUjianTerselesaikan.getSelectionModel().getSelectedItem();

        if (ujianTerpilih != null) {
            easyExam.showDetailSoal(ujianTerpilih, mahasiswa);
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Tidak ada pilihan");
            alert.setHeaderText("Anda tidak memilih");
            alert.setContentText("Silahkan pilih yang ingin anda lihat.");

            alert.showAndWait();
        }
    }
    
    @FXML
    public void handleGantiPassword(ActionEvent event){
        boolean okDitekan = easyExam.showGantiPassword(mahasiswa);
        if(okDitekan){
            //simpan
            easyExam.saveDataMahasiswa();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kolomJudulUjian.setCellValueFactory(cellData -> cellData.getValue().judulSoalProperty());
        kolomNilai.setCellValueFactory(cellData -> cellData.getValue().nilaiProperty());

        tabelUjianTerselesaikan.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> showDetailUjianSelesai(newValue));
    }

}
