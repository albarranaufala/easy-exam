/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class AboutUsController implements Initializable {
    private Stage stage;
    /**
     * Initializes the controller class.
     */
    
    public void setStage(Stage stage) {
        this.stage = stage;
    }    
    
    @FXML
    public void handleKembali(){
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
