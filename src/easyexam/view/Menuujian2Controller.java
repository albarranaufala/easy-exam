/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author User
 */
public class Menuujian2Controller implements Initializable {

    @FXML
    private RadioButton rdA;
    @FXML
    private RadioButton rdB;
    @FXML
    private RadioButton rdC;
    @FXML
    private RadioButton rdD;
    @FXML
    private Label nomorLabel;
    @FXML
    private Label lbSoal;
    @FXML
    private Button tombolLanjut;
    @FXML
    private Button tombolSebelumnya;
    @FXML
    private Label lbDetik;
    @FXML
    private Label lbMenit;
    @FXML
    private Label lbJam;

    private Stage menuUjian2Stage;
    private boolean okDitekan = false;
    private boolean setelah1 = false;
    private EasyExam easyExam;
    private Ujian ujian;
    private int nomorInt = 1;
    private Mahasiswa mahasiswa;
    private Text text;
    private Timeline animation;
    private int jam, menit, detik, detik1;

    public void setStage(Stage menuUjian2Stage) {
        this.menuUjian2Stage = menuUjian2Stage;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    //Supaya ganti-ganti radiobutton nya
    @FXML
    private void handlerdA(ActionEvent event) {
        rdB.setSelected(false);
        rdC.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdB(ActionEvent event) {
        rdA.setSelected(false);
        rdC.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdC(ActionEvent event) {
        rdB.setSelected(false);
        rdA.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdD(ActionEvent event) {
        rdB.setSelected(false);
        rdC.setSelected(false);
        rdA.setSelected(false);
    }

    public boolean isOkDitekan() {
        return okDitekan;
    }

    public void setUjian(Ujian ujian) {
        this.ujian = ujian;
        detik1 = ujian.getNilaiKetuntasan() * 60;
        String detik2 = (detik1 % 60) + "";
        String menit2 = ((detik1 / 60)%60) + "";
        String jam2 = (detik1 / 3600) + "";
        if(detik2.length()==1){
            detik2 = "0" + detik2;
        }
        if(menit2.length()==1){
            menit2 = "0" + menit2;
        }
        if(jam2.length()==1){
            jam2 = "0" + jam2;
        }
        lbDetik.setText(detik2);
        lbMenit.setText(menit2);
        lbJam.setText(jam2);
        animation = new Timeline(new KeyFrame(Duration.seconds(1), e -> timeLabel()));
        animation.setCycleCount(Timeline.INDEFINITE);
        animation.play();

    }

    public void timeLabel() {
        jam = detik1 / 3600;
        menit = detik1 / 60;
        menit = menit % 60;
        detik = detik1 % 60;
        detik1--;

        if (jam == 0 && menit == 0 && detik == 0) {
            int jawaban = jawaban();
            mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).setJawabanMhs(nomorInt - 1, jawaban);
            okDitekan = true;
            menuUjian2Stage.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(menuUjian2Stage);
            alert.setTitle("Ujian Selesai");
            alert.setHeaderText("Waktu Habis!");
            alert.setContentText("Anda mendapatkan nilai: " + mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).nilaiProperty().get());
            //BETULKAN!!!!
            ujian.getMahasiswaSelesai().add(mahasiswa);

            alert.show();
            animation.stop();

        }
        String detikStr = detik + "";
        String menitStr = menit + "";
        String jamStr = jam +"";
        if(detikStr.length()==1){
            detikStr = "0" + detikStr;
        }
        if(menitStr.length()==1){
            menitStr = "0" + menitStr;
        }
        if(jamStr.length()==1){
            jamStr = "0" + jamStr;
        }
        lbDetik.setText(detikStr);
        lbMenit.setText(menitStr);
        lbJam.setText(jamStr);
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;
    }

    public void rubahIsi(int indeks) {
        lbSoal.setText(ujian.getSoalsoal().get(indeks).getPertanyaan());
        rdA.setText(ujian.getSoalsoal().get(indeks).getPilA());
        rdB.setText(ujian.getSoalsoal().get(indeks).getPilB());
        rdC.setText(ujian.getSoalsoal().get(indeks).getPilC());
        rdD.setText(ujian.getSoalsoal().get(indeks).getPilD());

        if (mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).getJawabanMhs()[indeks] == 1) {
            rdA.setSelected(true);
            rdB.setSelected(false);
            rdC.setSelected(false);
            rdD.setSelected(false);
        } else if (mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).getJawabanMhs()[indeks] == 2) {
            rdA.setSelected(false);
            rdB.setSelected(true);
            rdC.setSelected(false);
            rdD.setSelected(false);
        } else if (mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).getJawabanMhs()[indeks] == 3) {
            rdA.setSelected(false);
            rdB.setSelected(false);
            rdC.setSelected(true);
            rdD.setSelected(false);
        } else if (mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).getJawabanMhs()[indeks] == 4) {
            rdA.setSelected(false);
            rdB.setSelected(false);
            rdC.setSelected(false);
            rdD.setSelected(true);
        } else {
            rdA.setSelected(false);
            rdB.setSelected(false);
            rdC.setSelected(false);
            rdD.setSelected(false);
        }

    }

    @FXML
    public void tombolLanjut(ActionEvent event) {
        int jawaban = jawaban();
        mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).setJawabanMhs(nomorInt - 1, jawaban);

        if (tombolLanjut.getText().equals("Selesai")) {
            boolean okDitekan1 = easyExam.showKonfirmasi();
            if (okDitekan1) {
                animation.stop();
                okDitekan = true;
                menuUjian2Stage.close();
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.initOwner(menuUjian2Stage);
                alert.setTitle("Ujian Selesai");
                alert.setHeaderText("Ujian Terselesaikan");
                alert.setContentText("Anda mendapatkan nilai: " + mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).nilaiProperty().get());
                //BETULKAN!!!!
                ujian.getMahasiswaSelesai().add(mahasiswa);
                alert.showAndWait();
            }

        } else {
            nomorInt++;

            setTombolSebelumDisable(nomorInt);

            rubahIsi(nomorInt - 1);

            if (ujian.getJumlahSoal() == nomorInt) {
                tombolLanjut.setText("Selesai");
            } else {
                tombolLanjut.setText("Lanjut");
            }

            nomorLabel.setText(Integer.toString(nomorInt));
        }

    }

    @FXML
    public void tombolSebelumnya(ActionEvent event) {
        int jawaban = jawaban();
        mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size() - 1).setJawabanMhs(nomorInt - 1, jawaban);

        nomorInt--;
        setTombolSebelumDisable(nomorInt);
        if (ujian.getJumlahSoal() != nomorInt) {
            tombolLanjut.setText("Lanjut");
        }
        rubahIsi(nomorInt - 1);

        nomorLabel.setText(Integer.toString(nomorInt));
    }

    public void gantiTulisanSelesaiSaatAwal() {
        if (ujian.getJumlahSoal() == 1) {
            tombolLanjut.setText("Selesai");
        }
    }

    public void setTombolSebelumDisable(int nomor) {
        if (nomor == 1) {
            tombolSebelumnya.setDisable(true);
        } else {
            tombolSebelumnya.setDisable(false);
        }
    }

    public int jawaban() {
        if (rdA.isSelected()) {
            return 1;
        } else if (rdB.isSelected()) {
            return 2;
        } else if (rdC.isSelected()) {
            return 3;
        } else if (rdD.isSelected()) {
            return 4;
        } else {
            return 0;
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomorLabel.setText(Integer.toString(nomorInt));

    }

}
