/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class Menuujian1Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    private EasyExam easyExam;    
    private Stage menuUjian1Stage;
    private Ujian ujian;
    private boolean okDitekan = false;
    private Mahasiswa mahasiswa;
    
    @FXML
    private Label lbJudulSoal;
    @FXML
    private Label lbBanyakSoal;
    @FXML
    private Label lbKetuntasan;
    
    public void setEasyExam(EasyExam easyExam){
        this.easyExam = easyExam;
    }
    
    public void setStage(Stage menuUjian1Stage){
        this.menuUjian1Stage = menuUjian1Stage;
    }
    
    public void setMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa = mahasiswa;
    }
    
    public void setUjian(Ujian ujian){
        this.ujian = ujian;
        
        lbJudulSoal.setText(ujian.getJudulUjian());
        lbBanyakSoal.setText(Integer.toString(ujian.getJumlahSoal()));
        lbKetuntasan.setText(Integer.toString(ujian.getNilaiKetuntasan()));
    }
    
    @FXML
    public void handleTombolMulai(ActionEvent event){
        okDitekan = true;
        mahasiswa.tambahUjianSelesai();
        mahasiswa.getUjianUjianSelesai().get(mahasiswa.getUjianUjianSelesai().size()-1).setUjian(ujian);
        boolean okDitekan1 = easyExam.showMenuMulaiUjian2(ujian,mahasiswa);
        if(okDitekan1) {
            this.menuUjian1Stage.close();
        }
    }
    
    public boolean isOkDitekan() {
        return okDitekan;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
