/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.model.Mahasiswa;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class DetailMahasiswaSelesaiController implements Initializable {

    private Stage stage;
    private Ujian ujian;

    @FXML
    private TableView<Mahasiswa> tabelMahasiswaSelesai;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNama;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNim;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNilai;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNilaiHuruf;

    @FXML
    private Label lbJudulSoal;
    @FXML
    private Label lbMean;
    @FXML
    private Label lbMedian;
    @FXML
    private Label lbModus;
    @FXML
    private Label lbVarians;
    @FXML
    private Label lbStandardDeviasi;
    @FXML
    private BarChart bcUjian; 
            
    XYChart.Series<String, Integer> series = new XYChart.Series();
    
    public void gantiLabelJudulSoal() {
        lbJudulSoal.setText(ujian.getJudulUjian());
    }

    public void setUjian(Ujian ujian) {
        this.ujian = ujian;
        
        kolomNilai.setCellValueFactory(cellData -> cellData.getValue().getUjianSelesaiTerpilih(ujian).nilaiProperty());
        kolomNilaiHuruf.setCellValueFactory(cellData -> cellData.getValue().getUjianSelesaiTerpilih(ujian).getNilaiHurufProperty());
        tabelMahasiswaSelesai.setItems(ujian.getMahasiswaSelesai());
        lbMean.setText(Double.toString(ujian.getMean()));
        lbModus.setText(Integer.toString(ujian.getModus()));
        lbVarians.setText(Double.toString(ujian.getVarians()));
        lbStandardDeviasi.setText(Double.toString(ujian.getStandardDeviasi()));
        lbMedian.setText(Double.toString(ujian.getMedian()));
                
        series.getData().add(new XYChart.Data<>("A", ujian.getJumlahA()));
        series.getData().add(new XYChart.Data<>("A-", ujian.getJumlahAmin()));
        series.getData().add(new XYChart.Data<>("A/B", ujian.getJumlahAperB()));
        series.getData().add(new XYChart.Data<>("B+", ujian.getJumlahBplus()));
        series.getData().add(new XYChart.Data<>("B", ujian.getJumlahB()));
        series.getData().add(new XYChart.Data<>("B-", ujian.getJumlahBmin()));
        series.getData().add(new XYChart.Data<>("B/C", ujian.getJumlahBperC()));
        series.getData().add(new XYChart.Data<>("C+", ujian.getJumlahCplus()));
        series.getData().add(new XYChart.Data<>("C", ujian.getJumlahC()));
        series.getData().add(new XYChart.Data<>("C-", ujian.getJumlahCmin()));
        series.getData().add(new XYChart.Data<>("C/D", ujian.getJumlahCperD()));
        series.getData().add(new XYChart.Data<>("D+", ujian.getJumlahDplus()));
        series.getData().add(new XYChart.Data<>("D", ujian.getJumlahD()));
        series.getData().add(new XYChart.Data<>("E", ujian.getJumlahE()));
        bcUjian.getData().add(series);
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }
    
    @FXML
    public void handleTombolKembali(ActionEvent event){
        stage.close();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kolomNama.setCellValueFactory(cellData -> cellData.getValue().namaProperty());
        kolomNim.setCellValueFactory(cellData -> cellData.getValue().nimProperty());
        
    }

}
