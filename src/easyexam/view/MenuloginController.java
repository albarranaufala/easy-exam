/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;


import com.jfoenix.controls.JFXRadioButton;
import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author User
 */
public class MenuloginController implements Initializable {

    /**
     * Initializes the controller class.
     */
    private EasyExam easyExam;
    private ObservableList<Mahasiswa> dataMahasiswa = FXCollections.observableArrayList();

    //deklarasi item
    @FXML
    private JFXRadioButton rdMahasiswa;
    @FXML
    private JFXRadioButton rdDosen;
    @FXML
    private TextField tfUsername;
    @FXML
    private PasswordField tfPassword;
    @FXML
    private Label lbPwSalah;

    public void setDataMahasiswa(ObservableList<Mahasiswa> dataMahasiswa) {
        this.dataMahasiswa = dataMahasiswa;
    }

    //metod radio button bisa ganti ganti
    @FXML
    public void rdMahasiswaHandle(ActionEvent event) {
        //buat radiobutton dosen menjadi false
        rdDosen.setSelected(false);
    }
    @FXML
    public void handleAboutUs(){
        easyExam.showAboutUs();
    }
    @FXML
    public void rdDosenHandle(ActionEvent event) {
        //buat radiobutton mahasiswa menjadi false
        rdMahasiswa.setSelected(false);
    }

    @FXML
    public void masukButton(ActionEvent event) {
        if (rdMahasiswa.isSelected()) {
            boolean akunDitemukan = false;
            int indeksMahasiswa = -1;
            for (int i = 0; i < dataMahasiswa.size(); i++) {
                dataMahasiswa.get(i).decryptPassword();
                if (dataMahasiswa.get(i).getNIM().equals(tfUsername.getText())
                        && dataMahasiswa.get(i).getPassword().equals(tfPassword.getText())) {
                    akunDitemukan = true;
                    indeksMahasiswa = i;
                }
                dataMahasiswa.get(i).encryptPassword();
            }
           
            if (akunDitemukan) {
                easyExam.showMenuMahasiswa(dataMahasiswa.get(indeksMahasiswa));
            } else {
                lbPwSalah.setText("username atau password salah");
            }
        } else if (rdDosen.isSelected()) {
            boolean akunDitemukan = false;
            int indeksDosen = -1;
            for(int i =0; i< easyExam.getListDosen().size(); i++){
                easyExam.getListDosen().get(i).decryptPassword();
                if(easyExam.getListDosen().get(i).getNID().equals(tfUsername.getText())
                        && easyExam.getListDosen().get(i).getPassword().equals(tfPassword.getText())){
                    akunDitemukan = true;
                    indeksDosen = i;
                }
                easyExam.getListDosen().get(i).encryptPassword();
            }
            
            if(akunDitemukan){
                easyExam.showMenuDosen(easyExam.getListDosen().get(indeksDosen));
            } else{
                lbPwSalah.setText("username atau password salah");
            }
            
        } else {
           lbPwSalah.setText("silahkan pilih jenis akun");
        }
    }

    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
