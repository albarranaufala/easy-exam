/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.model.Mahasiswa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class GantiPasswordController implements Initializable {

    private Mahasiswa mahasiswa;
    private Stage stage;
    private boolean okDitekan = false;
    @FXML
    private TextField tfPwLama;
    @FXML
    private TextField tfPwBaru;
    
    public void setMahasiswa(Mahasiswa mahasiswa){
        this.mahasiswa = mahasiswa;
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    @FXML
    public void handleOK(ActionEvent event){
        if(isInputValid()){
            okDitekan = true;
            mahasiswa.decryptPassword();
            mahasiswa.setPassword(tfPwBaru.getText());
            mahasiswa.encryptPassword();
            
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(stage);
            alert.setTitle("Berhasil");
            alert.setHeaderText("Mengganti Password Berhasil");
            alert.setContentText("Selamat, anda berhasil mengganti password!");
            
            alert.showAndWait();
            
            stage.close();
        }
    }
    
    @FXML
    public void handleBatal(ActionEvent event){
        stage.close();
    }
    
    private boolean isInputValid() {
        String errorMessage = "";

        if (tfPwLama.getText() == null || tfPwLama.getText().length() == 0) {
            errorMessage += "Password lama tidak valid!\n"; 
        }
        if (tfPwBaru.getText() == null || tfPwBaru.getText().length() == 0) {
            errorMessage += "Password baru tidak valid!\n"; 
        }
        mahasiswa.decryptPassword();
        if(!tfPwLama.getText().equals(mahasiswa.getPassword())){
            errorMessage += "Password lama salah!\n";
        }
        mahasiswa.encryptPassword();
        
        if (errorMessage.length() == 0) {
            return true;
        } else {
            // nampilin pesan error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(stage);
            alert.setTitle("Kesalahan Mengisi");
            alert.setHeaderText("Silahkan isi form dengan valid");
            alert.setContentText(errorMessage);
            
            alert.showAndWait();
            
            return false;
        }
    }
    
    public boolean isOkDitekan(){
        return this.okDitekan;
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
