/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Mahasiswa;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class MenuDataMahasiswaController implements Initializable {

    private EasyExam easyExam;
    private Stage stage;
    @FXML
    private TableView<Mahasiswa> tabelMahasiswa;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNama;
    @FXML
    private TableColumn<Mahasiswa, String> kolomNim;
    
    
    public void setEasyExam(EasyExam easyExam){
        this.easyExam = easyExam;
        
        tabelMahasiswa.setItems(easyExam.getDataMahasiswa());
    }
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    @FXML
    public void handleTombolKembali(ActionEvent event){
       stage.close();
    }
    
    @FXML
    private void handleTombolHapus() {
        int selectedIndex = tabelMahasiswa.getSelectionModel().getSelectedIndex();
        if (selectedIndex >= 0) {
            tabelMahasiswa.getItems().remove(selectedIndex);
            //simpan perubahan
            easyExam.saveDataMahasiswa();
        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Tidak ada pilihan");
            alert.setHeaderText("Anda tidak memilih");
            alert.setContentText("Silahkan pilih yang mau anda hapus.");

            alert.showAndWait();
        }
    }
    
    @FXML
    private void handleTambahMahasiswa(ActionEvent event){
        Mahasiswa tmp = new Mahasiswa();
        boolean okDitekan = easyExam.showTambahMahasiswa(tmp);
        if(okDitekan){
            //simpan
            easyExam.getDataMahasiswa().add(tmp);
            easyExam.saveDataMahasiswa();
        }
    }
    
    @FXML
    private void handleEditMahasiswa() {
        Mahasiswa mahasiswaTerpilih = tabelMahasiswa.getSelectionModel().getSelectedItem();
        if (mahasiswaTerpilih != null) {
            boolean okClicked = easyExam.showTambahMahasiswa(mahasiswaTerpilih);
            if (okClicked) {
                easyExam.saveDataMahasiswa();
            }

        } else {
            // Nothing selected.
            Alert alert = new Alert(Alert.AlertType.WARNING);
            alert.initOwner(easyExam.getPrimaryStage());
            alert.setTitle("Tidak ada pilihan");
            alert.setHeaderText("Tidak ada yang anda pilih");
            alert.setContentText("Silahkan pilih yang mau diedit.");
            
            alert.showAndWait();
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kolomNama.setCellValueFactory(cellData -> cellData.getValue().namaProperty());
        kolomNim.setCellValueFactory(cellData -> cellData.getValue().nimProperty());
    }    
    
}
