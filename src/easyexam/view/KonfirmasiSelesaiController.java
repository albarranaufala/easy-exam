/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class KonfirmasiSelesaiController implements Initializable {

    private Stage stage;
    private boolean okDitekan = false;
    
    public void setStage(Stage stage){
        this.stage = stage;
    }
    
    @FXML
    public void handleOK(ActionEvent event){
        okDitekan = true;
        stage.close();
    } 
    
    @FXML
    public void handleBatal(ActionEvent event){
        stage.close();
    }
    
    public boolean isOkDitekan(){
        return this.okDitekan;
    }
            
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
