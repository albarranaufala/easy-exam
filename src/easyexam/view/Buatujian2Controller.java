/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Ujian;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class Buatujian2Controller implements Initializable {

    /**
     * Initializes the controller class.
     */
    private EasyExam easyExam;
    private Ujian ujian;
    private Stage Buatujian2Stage;
    private int nomorInt = 1;
    private boolean okDitekan = false;

    @FXML
    private Label nomorLabel;
    @FXML
    private TextArea taSoal;
    @FXML
    private RadioButton rdA;
    @FXML
    private RadioButton rdB;
    @FXML
    private RadioButton rdC;
    @FXML
    private RadioButton rdD;
    @FXML
    private TextField tfA;
    @FXML
    private TextField tfB;
    @FXML
    private TextField tfC;
    @FXML
    private TextField tfD;

    @FXML
    private Button tombolLanjut;
    @FXML
    private Button tombolSebelumnya;


    public void setEasyExam(EasyExam easyExam) {
        this.easyExam = easyExam;
    }

    public boolean isokDitekan() {
        return okDitekan;
    }

//    @FXML
//    public void handleEdit(ActionEvent event){
//        simpanIsi(nomorInt - 1);
//        boolean okDitekan = easyExam.showBuatUjian1(ujian);
//        
//    }
    
    @FXML
    public void tombolLanjut(ActionEvent event) {
        ujian.tambahSoal();
        simpanIsi(nomorInt - 1);
        if (tombolLanjut.getText().equals("Simpan")) {
            okDitekan = true;
            Buatujian2Stage.close();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(Buatujian2Stage);
            alert.setTitle("Ujian Tersimpan");
            alert.setHeaderText("Ujian Tersimpan");
            alert.setContentText("Selamat ujian berhasil tersimpan!");
            
            alert.showAndWait();
        } else {
            nomorInt++;
            setTombolSebelumDisable(nomorInt);
            if (ujian.getSoalsoal().size() <= nomorInt - 1) {

                kosongkanIsi();
            } else {
                rubahIsi(nomorInt - 1);
            }

            if (ujian.getJumlahSoal() == nomorInt) {
                tombolLanjut.setText("Simpan");
            } else {
                tombolLanjut.setText("Lanjut");
            }

            nomorLabel.setText(Integer.toString(nomorInt));
        }

    }

//    @FXML
//    public void handleEditInformasi(){
//        boolean okDitekan = easyExam.showBuatUjian1(ujian, true);
//        if(okDitekan){
//            Buatujian2Stage.close();
//            easyExam.saveDataTabel();
//        }
//    }
    
    @FXML
    public void tombolSebelumnya(ActionEvent event) {

        ujian.tambahSoal();
        simpanIsi(nomorInt - 1);
        nomorInt--;
        setTombolSebelumDisable(nomorInt);
        if (ujian.getJumlahSoal() != nomorInt) {
            tombolLanjut.setText("Lanjut");
        }
        rubahIsi(nomorInt - 1);

        nomorLabel.setText(Integer.toString(nomorInt));
    }

    //Supaya ganti-ganti radiobutton nya
    @FXML
    private void handlerdA(ActionEvent event) {
        rdB.setSelected(false);
        rdC.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdB(ActionEvent event) {
        rdA.setSelected(false);
        rdC.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdC(ActionEvent event) {
        rdB.setSelected(false);
        rdA.setSelected(false);
        rdD.setSelected(false);
    }

    @FXML
    private void handlerdD(ActionEvent event) {
        rdB.setSelected(false);
        rdC.setSelected(false);
        rdA.setSelected(false);
    }

    public void setStage(Stage Buatujian2Stage) {
        this.Buatujian2Stage = Buatujian2Stage;
    }

    public void setUjian(Ujian ujian) {
        this.ujian = ujian;
    }

    public void simpanIsi(int indeks) {
        ujian.getSoalsoal().get(indeks).setPertanyaan(taSoal.getText());
        ujian.getSoalsoal().get(indeks).setPilA(tfA.getText());
        ujian.getSoalsoal().get(indeks).setPilB(tfB.getText());
        ujian.getSoalsoal().get(indeks).setPilC(tfC.getText());
        ujian.getSoalsoal().get(indeks).setPilD(tfD.getText());
        if (rdA.isSelected()) {
            ujian.getSoalsoal().get(indeks).setKunciJawaban(1);
        } else if (rdB.isSelected()) {
            ujian.getSoalsoal().get(indeks).setKunciJawaban(2);
        } else if (rdC.isSelected()) {
            ujian.getSoalsoal().get(indeks).setKunciJawaban(3);
        } else if (rdD.isSelected()) {
            ujian.getSoalsoal().get(indeks).setKunciJawaban(4);
        }
    }

    public void kosongkanIsi() {
        taSoal.setText("");
        tfA.setText("");
        tfB.setText("");
        tfC.setText("");
        tfD.setText("");
        rdA.setSelected(false);
        rdB.setSelected(false);
        rdC.setSelected(false);
        rdD.setSelected(false);
    }

    public void rubahIsi(int indeks) {;
        taSoal.setText(ujian.getSoalsoal().get(indeks).getPertanyaan());
        tfA.setText(ujian.getSoalsoal().get(indeks).getPilA());
        tfB.setText(ujian.getSoalsoal().get(indeks).getPilB());
        tfC.setText(ujian.getSoalsoal().get(indeks).getPilC());
        tfD.setText(ujian.getSoalsoal().get(indeks).getPilD());
        
        if (ujian.getSoalsoal().get(indeks).getKunciJawaban() == 1) {
            rdA.setSelected(true);
            rdB.setSelected(false);
            rdC.setSelected(false);
            rdD.setSelected(false);
        } else if (ujian.getSoalsoal().get(indeks).getKunciJawaban() == 2) {
            rdA.setSelected(false);
            rdB.setSelected(true);
            rdC.setSelected(false);
            rdD.setSelected(false);
        } else if (ujian.getSoalsoal().get(indeks).getKunciJawaban() == 3) {
            rdA.setSelected(false);
            rdB.setSelected(false);
            rdC.setSelected(true);
            rdD.setSelected(false);
        } else if (ujian.getSoalsoal().get(indeks).getKunciJawaban() == 4) {
            rdA.setSelected(false);
            rdB.setSelected(false);
            rdC.setSelected(false);
            rdD.setSelected(true);
        }
    }

    public void gantiTulisanSelesaiSaatAwal(){
        if(ujian.getJumlahSoal()==1){
            tombolLanjut.setText("Simpan");
        }
    }
    
    public void setTombolSebelumDisable(int nomor){
        if(nomor==1){
            tombolSebelumnya.setDisable(true);
        }
        else{
            tombolSebelumnya.setDisable(false);
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        nomorLabel.setText(Integer.toString(nomorInt));
    }

}
