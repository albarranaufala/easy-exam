/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.model.Mahasiswa;
import easyexam.model.Soal;
import easyexam.model.Ujian;
import easyexam.model.UjianSelesai;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class DetailSoalSelesaiController implements Initializable {

    private UjianSelesai ujianSelesai;
    private Mahasiswa mahasiswa;
    private Stage stage;

    @FXML
    private Label lbJudul;
    @FXML
    private TableView<Soal> tabelUjianSelesai;
//    @FXML
//    private TableColumn<Soal, Integer> kolomNomor;
    @FXML
    private TableColumn<Soal, String> kolomPertanyaan;
//    @FXML
//    private TableColumn<Soal, String> kolomKebenaran;
    

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setUjianSelesai(UjianSelesai ujianSelesai) {
        this.ujianSelesai = ujianSelesai;

    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;

        kolomPertanyaan.setCellValueFactory(cellData -> cellData.getValue().pertanyaanProperty());
        // kolomKebenaran.setCellValueFactory(cellData -> cellData.getValue().judulSoalProperty());
        tabelUjianSelesai.setItems(mahasiswa.getUjianSelesaiTerpilih(ujianSelesai).getSoalSoalObservable());
        
    }

    public void gantiLabel() {
        lbJudul.setText(ujianSelesai.getJudulUjian());
    }

    @FXML
    public void handleTombolKembali(ActionEvent event) {
        stage.close();
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

}
