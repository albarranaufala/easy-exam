/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.view;

import easyexam.EasyExam;
import easyexam.model.Dosen;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author User
 */
public class EditDataDosenController implements Initializable {

    private Stage stage;
    private Dosen dosen;
    private boolean okDitekan = false;
    private EasyExam easyExam;
    
    public void setEasyExam(EasyExam easyExam){
        this.easyExam = easyExam;
    }

    @FXML
    private TextField tfNama;
    @FXML
    private TextField tfNID;

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setDosen(Dosen dosen) {
        this.dosen = dosen;
        tfNama.setText(dosen.getNama());
        tfNID.setText(dosen.getNID());

    }

    @FXML
    public void handleOK(ActionEvent event) {
        if (isInputValid()) {
            okDitekan = true;
            dosen.setNama(tfNama.getText());
            dosen.setNID(tfNID.getText());

            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.initOwner(stage);
            alert.setTitle("Berhasil");
            alert.setHeaderText("Menyimpan Data Berhasil");
            alert.setContentText("Selamat, anda berhasil menyimpan data!");

            alert.showAndWait();

            stage.close();
        }
    }

    @FXML
    public void handleBatal(ActionEvent event) {
        stage.close();
    }

    @FXML
    public void handleGantiPassword(ActionEvent event) {
        boolean okDitekan = easyExam.showGantiPasswordDosen(dosen);
        if (okDitekan) {
            //simpan
            easyExam.saveListDosen();
        }
    }

    private boolean isInputValid() {
        String errorMessage = "";

        if (tfNama.getText() == null || tfNama.getText().length() == 0) {
            errorMessage += "Nama tidak valid!\n";
        }
        if (tfNID.getText() == null || tfNID.getText().length() == 0) {
            errorMessage += "NID tidak valid!\n";
        }

        if (errorMessage.length() == 0) {
            return true;
        } else {
            // nampilin pesan error
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.initOwner(stage);
            alert.setTitle("Kesalahan Mengisi");
            alert.setHeaderText("Silahkan isi form dengan valid");
            alert.setContentText(errorMessage);

            alert.showAndWait();

            return false;
        }
    }

    public boolean isOkDitekan() {
        return okDitekan;
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
