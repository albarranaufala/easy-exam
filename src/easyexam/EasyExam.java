
/*
Albarra Naufala Erdanto 18523158
Program yang dibuat: EasyExam.java, MenuUjian1, MenuUjian2, Dosen.java, Mahasiswa.java, Konfirmasi Selesai
Kesulitan: Mencari cara-cara yang tidak diajarkan di kelas di internet.(Solved)

Ferdian Nursulistio 18523149
Program yang dibuat : MenuLogin, MenuAwalMahasiswa, MahasiswaListWrapper.java, Algoritma timer countdown
Kesulitan : Mencari cara menulis, membaca, dan mengedit XML dan komunikasi antar controller(Solved)

Adam Nurfaizi 18523159
Program yang dibuat : Detail ujian, Detail soal, Ujian.java, Statistik detail Mahasiswa selesai.
Kesulitan : kesulitan diawal ketika belum mengetahui cara mengoperasikan XML.(Solved)

Andi Rivaldo Rambe 18523151
Program yang dibuat : Soal.java, Buatujian1, Buatujian2, Tambah mahasiswa, Menu data Mahasiswa
Kesulitan : membuat logika pilihan jawaban agar tersimpan(Solved)

Nugroho Fadillah Yudha Putra 18523156
Program yang dibuat : DosenListWrapper.java, Menuawaldosen, EditDataDosen, Ganti Password
Kesulitan : Memasukkan data nilai mahasiswa ke PieChart (Solved)
*/

package easyexam;

import easyexam.model.Ujian;
import easyexam.view.Buatujian1Controller;
import easyexam.view.MenuawaldosenController;
import easyexam.view.MenuawalmahasiswaController;
import easyexam.view.Menuujian1Controller;
import easyexam.view.Menuujian2Controller;
import easyexam.view.Buatujian2Controller;
import java.io.FileOutputStream;
import java.io.IOException;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
// untuk menangani format XML
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import easyexam.model.Dosen;
import easyexam.model.DosenListWrapper;
import easyexam.model.Mahasiswa;
import easyexam.model.MahasiswaListWrapper;
import easyexam.model.UjianListWrapper;
import easyexam.model.UjianSelesai;
import easyexam.view.AboutUsController;
import easyexam.view.DetailMahasiswaSelesaiController;
import easyexam.view.DetailSoalSelesaiController;
import easyexam.view.EditDataDosenController;
import easyexam.view.GantiPasswordController;
import easyexam.view.GantiPasswordDosenController;
import easyexam.view.KonfirmasiSelesaiController;
import easyexam.view.MenuDataMahasiswaController;
import easyexam.view.MenuloginController;
import easyexam.view.TambahMahasiswaController;
import java.io.FileInputStream;


public class EasyExam extends Application {
    
    private Stage primaryStage;
    private ObservableList<Ujian> dataUjian = FXCollections.observableArrayList();
    private ObservableList<Mahasiswa> dataMahasiswa = FXCollections.observableArrayList();
    private ObservableList<Dosen> listDosen = FXCollections.observableArrayList();

    public EasyExam() {
//        listDosen.add(new Dosen("dosen", "dosen"));
//        dataMahasiswa.add(new Mahasiswa("Affan Taufiqur Abroor", "18523100"));
//        dataMahasiswa.add(new Mahasiswa("Farhan Ramadhan", "18523104"));
//        dataMahasiswa.add(new Mahasiswa("Pradipta Kurniawan", "18523105"));
//        dataMahasiswa.add(new Mahasiswa("Fadila Ahmad Syabani", "18523107"));
//        dataMahasiswa.add(new Mahasiswa("Ferdian Nursulistio", "18523149"));
//        dataMahasiswa.add(new Mahasiswa("Nugroho Fadillah Yudha Putra", "18523156"));
//        dataMahasiswa.add(new Mahasiswa("Andi Rivaldo Rambe", "18523151"));
//        dataMahasiswa.add(new Mahasiswa("Albarra Naufala Erdanto", "18523158"));
//        dataMahasiswa.add(new Mahasiswa("Adam Nurfaizi", "18523159"));
//        dataMahasiswa.add(new Mahasiswa("Fadhlan Failusuf Salam", "18523142"));
//        dataMahasiswa.add(new Mahasiswa("Hitawasana Adhyatma Atmadi", "18523106"));
//        dataMahasiswa.add(new Mahasiswa("Sigit Priadi", "18523125"));
//        dataMahasiswa.add(new Mahasiswa("Muhammad Farhan", "18523099"));
    }

    public ObservableList<Ujian> getDataUjian() {
        return dataUjian;
    }

    public ObservableList<Mahasiswa> getDataMahasiswa() {
        return dataMahasiswa;
    }
    
    public ObservableList<Dosen> getListDosen() {
        return listDosen;
    }    
    
    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("Easy Exam");

        showMenuLogin();
    }

    public void showMenuLogin() {
        try {

            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Menulogin.fxml"));
            AnchorPane menuLogin = (AnchorPane) loader.load();
            
            
            Scene scene = new Scene(menuLogin);
            primaryStage.setScene(scene);

            // Give the controller access to the main app.
            MenuloginController controller = loader.getController();
            controller.setEasyExam(this);
            this.loadDataMahasiswa();
            this.loadDataDosen();
            controller.setDataMahasiswa(dataMahasiswa);
            
            
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMenuDosen(Dosen dosen) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Menuawaldosen.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuAwalStage = new Stage();
            menuAwalStage.setTitle("Menu Awal Dosen");
            menuAwalStage.initModality(Modality.WINDOW_MODAL);
            menuAwalStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuAwalStage.setScene(scene);

            primaryStage.close(); //menutup login menu

            //membuat controller
            MenuawaldosenController controller = loader.getController();
            controller.setMenuAwalStage(menuAwalStage);
            controller.setEasyExam(this);
            controller.setDosen(dosen);
            this.loadDataTabel();
            
            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuAwalStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void showMenuMahasiswa(Mahasiswa mahasiswa) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Menuawalmahasiswa.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuAwalStage = new Stage();
            menuAwalStage.setTitle("Menu Awal Mahasiswa");
            menuAwalStage.initModality(Modality.WINDOW_MODAL);
            menuAwalStage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuAwalStage.setScene(scene);

            primaryStage.close(); //menutup login menu

            //membuat controller
            MenuawalmahasiswaController controller = loader.getController();
            controller.setMenuAwalStage(menuAwalStage);
            controller.setEasyExam(this);
            controller.setMahasiswa(mahasiswa);
            controller.setTulisanNama();
            this.loadDataTabel();
            
            
            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuAwalStage.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showAboutUs() {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/AboutUs.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menu = new Stage();
            menu.setTitle("About Us");
            menu.initModality(Modality.WINDOW_MODAL);
            menu.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menu.setScene(scene);

            //membuat controller
            AboutUsController controller = loader.getController();
            controller.setStage(menu);
            
            
            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menu.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void showDataMahasiswa() {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/MenuDataMahasiswa.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuDataMhs = new Stage();
            menuDataMhs.setTitle("Data Mahasiswa");
            menuDataMhs.initModality(Modality.WINDOW_MODAL);
            menuDataMhs.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuDataMhs.setScene(scene);

            //membuat controller
            MenuDataMahasiswaController controller = loader.getController();
            controller.setStage(menuDataMhs);
            controller.setEasyExam(this);
            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuDataMhs.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean showGantiPassword(Mahasiswa mahasiswa) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/GantiPassword.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuGntiPw = new Stage();
            menuGntiPw.setTitle("Ganti Password");
            menuGntiPw.initModality(Modality.WINDOW_MODAL);
            menuGntiPw.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuGntiPw.setScene(scene);

            //membuat controller
            GantiPasswordController controller = loader.getController();
            controller.setStage(menuGntiPw);
            controller.setMahasiswa(mahasiswa);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuGntiPw.showAndWait();
            
            return controller.isOkDitekan();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean showGantiPasswordDosen(Dosen dosen) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/GantiPasswordDosen.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuGntiPw = new Stage();
            menuGntiPw.setTitle("Ganti Password");
            menuGntiPw.initModality(Modality.WINDOW_MODAL);
            menuGntiPw.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuGntiPw.setScene(scene);

            //membuat controller
            GantiPasswordDosenController controller = loader.getController();
            controller.setStage(menuGntiPw);
            controller.setDosen(dosen);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuGntiPw.showAndWait();
            
            return controller.isOkDitekan();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showEditData(Dosen dosen) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/EditDataDosen.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuEdit = new Stage();
            menuEdit.setTitle("Edit Data Diri");
            menuEdit.initModality(Modality.WINDOW_MODAL);
            menuEdit.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuEdit.setScene(scene);

            //membuat controller
            EditDataDosenController controller = loader.getController();
            controller.setStage(menuEdit);
            controller.setEasyExam(this);
            controller.setDosen(dosen);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuEdit.showAndWait();
            
            return controller.isOkDitekan();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void showDetailMahasiswaDetail(Ujian ujian) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/DetailMahasiswaSelesai.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuDetailMhs = new Stage();
            menuDetailMhs.setTitle("Menu Mahasiswa Selesai");
            menuDetailMhs.initModality(Modality.WINDOW_MODAL);
            menuDetailMhs.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuDetailMhs.setScene(scene);

            //membuat controller
            DetailMahasiswaSelesaiController controller = loader.getController();
            controller.setStage(menuDetailMhs);
            controller.setUjian(ujian);
            controller.gantiLabelJudulSoal();
            

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuDetailMhs.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean showTambahMahasiswa(Mahasiswa mahasiswa) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/TambahMahasiswa.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage
            Stage menuTambahMahasiswa = new Stage();
            menuTambahMahasiswa.setTitle("Tambah Mahasiswa");
            menuTambahMahasiswa.initModality(Modality.WINDOW_MODAL);
            menuTambahMahasiswa.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuTambahMahasiswa.setScene(scene);

            //membuat controller
            TambahMahasiswaController controller = loader.getController();
            controller.setStage(menuTambahMahasiswa);
            controller.setEasyExam(this);
            controller.setDataMahasiswa(dataMahasiswa);
            controller.setMahasiswa(mahasiswa);
            

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuTambahMahasiswa.showAndWait();

            return controller.isOkDitekan();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public void showDetailSoal(UjianSelesai ujianSelesai, Mahasiswa mahasiswa){
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/DetailSoalSelesai.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu awal
            Stage menuDetailSoal = new Stage();
            menuDetailSoal.setTitle("Menu Soal Selesai");
            menuDetailSoal.initModality(Modality.WINDOW_MODAL);
            menuDetailSoal.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuDetailSoal.setScene(scene);

            //membuat controller
            DetailSoalSelesaiController controller = loader.getController();
            controller.setStage(menuDetailSoal);
            controller.setUjianSelesai(ujianSelesai);
            controller.setMahasiswa(mahasiswa);
            controller.gantiLabel();
            

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuDetailSoal.showAndWait();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public boolean showMenuMulaiUjian1(Ujian ujian, Mahasiswa mahasiswa) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Menuujian1.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu mulai ujian
            Stage menuMulaiUjian1Stage = new Stage();
            menuMulaiUjian1Stage.setTitle("Mulai Ujian");
            menuMulaiUjian1Stage.initModality(Modality.WINDOW_MODAL);
            menuMulaiUjian1Stage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuMulaiUjian1Stage.setScene(scene);

            //membuat controller
            Menuujian1Controller controller = loader.getController();
            controller.setStage(menuMulaiUjian1Stage);
            controller.setEasyExam(this);
            controller.setUjian(ujian);
            controller.setMahasiswa(mahasiswa);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuMulaiUjian1Stage.showAndWait();

            return controller.isOkDitekan();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showMenuMulaiUjian2(Ujian ujian, Mahasiswa mahasiswa) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Menuujian2.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu mulai ujian
            Stage menuMulaiUjian2Stage = new Stage();
            menuMulaiUjian2Stage.setTitle("Ujian");
            menuMulaiUjian2Stage.initModality(Modality.WINDOW_MODAL);
            menuMulaiUjian2Stage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            menuMulaiUjian2Stage.setScene(scene);

            //membuat controller
            Menuujian2Controller controller = loader.getController();
            controller.setStage(menuMulaiUjian2Stage);
            controller.setEasyExam(this);
            controller.setUjian(ujian);
            controller.setMahasiswa(mahasiswa);
            controller.gantiTulisanSelesaiSaatAwal();

            controller.rubahIsi(0);

            controller.setTombolSebelumDisable(1);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            menuMulaiUjian2Stage.showAndWait();

            return controller.isOkDitekan();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public boolean showKonfirmasi() {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/KonfirmasiSelesai.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu mulai ujian
            Stage konfirmasi = new Stage();
            konfirmasi.setTitle("Konfirmasi");
            konfirmasi.initModality(Modality.WINDOW_MODAL);
            konfirmasi.initOwner(primaryStage);
            Scene scene = new Scene(page);
            konfirmasi.setScene(scene);

            //membuat controller
            KonfirmasiSelesaiController controller = loader.getController();
            controller.setStage(konfirmasi);
            //controller.setEasyExam(this);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            konfirmasi.showAndWait();

            return controller.isOkDitekan();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showBuatUjian1(Ujian ujian) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Buatujian1.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage menu buat ujian
            Stage buatUjian1Stage = new Stage();
            buatUjian1Stage.setTitle("Buat Ujian");
            buatUjian1Stage.initModality(Modality.WINDOW_MODAL);
            buatUjian1Stage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            buatUjian1Stage.setScene(scene);

            //membuat controller
            Buatujian1Controller controller = loader.getController();
            controller.setStage(buatUjian1Stage);
            controller.setEasyExam(this);
            controller.setUjian(ujian);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            buatUjian1Stage.showAndWait();

            return controller.isokDitekan();

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean showBuatUjian2(Ujian ujian) {
        try {
            // Load the fxml file and create a new stage
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(EasyExam.class.getResource("view/Buatujian2.fxml"));
            AnchorPane page = (AnchorPane) loader.load();

            // membuat stage ujian
            Stage buatUjian2Stage = new Stage();
            buatUjian2Stage.setTitle("Buat Ujian");
            buatUjian2Stage.initModality(Modality.WINDOW_MODAL);
            buatUjian2Stage.initOwner(primaryStage);
            Scene scene = new Scene(page);
            buatUjian2Stage.setScene(scene);

            //membuat controller
            Buatujian2Controller controller = loader.getController();
            controller.setStage(buatUjian2Stage);
            controller.setEasyExam(this);
            controller.setUjian(ujian);
            controller.gantiTulisanSelesaiSaatAwal();

            if (ujian.getSoalsoal().size() != 0) {
                controller.rubahIsi(0);
            }
            controller.setTombolSebelumDisable(1);

            // Set the dialog icon.
            // dialogStage.getIcons().add(new Image("file:resources/images/edit.png"));
            // Show the dialog and wait until the user closes it
            buatUjian2Stage.showAndWait();

            return controller.isokDitekan();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public void saveDataTabel() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        UjianListWrapper wrapper = new UjianListWrapper();

        wrapper.setUjianUjian(dataUjian);
        // larik List diubah menjadi string dengan format XML
        String xml = xstream.toXML(wrapper);

        FileOutputStream fileUjian = null;
        try {
            // membuat nama file & folder tempat menyimpan jika perlu
            fileUjian = new FileOutputStream("fileUjian.xml");

            // mengubah karakter penyusun string xml sebagai 
            // bytes (berbentuk nomor2 kode ASCII
            byte[] bytes = xml.getBytes("UTF-8");

            // menyimpan file dari bytes
            fileUjian.write(bytes);
        } catch (Exception e) {
            System.err.println("Perhatian: " + e.getMessage());
        } finally {
            if (fileUjian != null) {
                try {
                    fileUjian.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void loadDataTabel() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        FileInputStream fileUjian = null;
        try {

            // nama file yang akan dibuka (termasuk folder jika perlu
            fileUjian = new FileInputStream("fileUjian.xml");

            // harus diingat objek apa yang dahulu disimpan di file 
            // program untuk membaca harus sinkron dengan program
            // yang dahulu digunakan untuk menyimpannya
            int isi;
            char charnya;
            String stringnya;

            // isi file dikembalikan menjadi string
            stringnya = "";
            while ((isi = fileUjian.read()) != -1) {
                charnya = (char) isi;
                stringnya = stringnya + charnya;
            }

            // string isi file dikembalikan menjadi larik double
            UjianListWrapper wrapper = new UjianListWrapper();
            wrapper = (UjianListWrapper) xstream.fromXML(stringnya);

            dataUjian.clear();
            dataUjian.addAll(wrapper.getUjianUjian());

        } catch (Exception e) {
            System.err.println("test: " + e.getMessage());
        } finally {
            if (fileUjian != null) {
                try {
                    fileUjian.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void saveDataMahasiswa() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        MahasiswaListWrapper wrapper = new MahasiswaListWrapper();

        wrapper.setMahasiswaMahasiswa(dataMahasiswa);
        // larik List diubah menjadi string dengan format XML
        String xml = xstream.toXML(wrapper);

        FileOutputStream fileMahasiswa = null;
        try {
            // membuat nama file & folder tempat menyimpan jika perlu
            fileMahasiswa = new FileOutputStream("fileMahasiswa.xml");

            // mengubah karakter penyusun string xml sebagai 
            // bytes (berbentuk nomor2 kode ASCII
            byte[] bytes = xml.getBytes("UTF-8");

            // menyimpan file dari bytes
            fileMahasiswa.write(bytes);
        } catch (Exception e) {
            System.err.println("Perhatian: " + e.getMessage());
        } finally {
            if (fileMahasiswa != null) {
                try {
                    fileMahasiswa.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void loadDataMahasiswa() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        FileInputStream fileMahasiswa = null;
        try {

            // nama file yang akan dibuka (termasuk folder jika perlu
            fileMahasiswa = new FileInputStream("fileMahasiswa.xml");

            // harus diingat objek apa yang dahulu disimpan di file 
            // program untuk membaca harus sinkron dengan program
            // yang dahulu digunakan untuk menyimpannya
            int isi;
            char charnya;
            String stringnya;

            // isi file dikembalikan menjadi string
            stringnya = "";
            while ((isi = fileMahasiswa.read()) != -1) {
                charnya = (char) isi;
                stringnya = stringnya + charnya;
            }

            // string isi file dikembalikan menjadi larik double
            MahasiswaListWrapper wrapper = new MahasiswaListWrapper();
            wrapper = (MahasiswaListWrapper) xstream.fromXML(stringnya);

            dataMahasiswa.clear();
            dataMahasiswa.addAll(wrapper.getMahasiswaMahasiswa());

        } catch (Exception e) {
            System.err.println("test: " + e.getMessage());
        } finally {
            if (fileMahasiswa != null) {
                try {
                    fileMahasiswa.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void loadDataDosen() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        FileInputStream fileDosen = null;
        try {

            // nama file yang akan dibuka (termasuk folder jika perlu
            fileDosen = new FileInputStream("fileDosen.xml");

            // harus diingat objek apa yang dahulu disimpan di file 
            // program untuk membaca harus sinkron dengan program
            // yang dahulu digunakan untuk menyimpannya
            int isi;
            char charnya;
            String stringnya;

            // isi file dikembalikan menjadi string
            stringnya = "";
            while ((isi = fileDosen.read()) != -1) {
                charnya = (char) isi;
                stringnya = stringnya + charnya;
            }

            // string isi file dikembalikan menjadi larik double
            DosenListWrapper wrapper = new DosenListWrapper();
            wrapper = (DosenListWrapper) xstream.fromXML(stringnya);

            listDosen.clear();
            listDosen.addAll(wrapper.getDosenDosen());

        } catch (Exception e) {
            System.err.println("test: " + e.getMessage());
        } finally {
            if (fileDosen != null) {
                try {
                    fileDosen.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void saveListDosen() {
        XStream xstream = new XStream(new StaxDriver());
        xstream.addPermission(AnyTypePermission.ANY); 
        DosenListWrapper wrapper = new DosenListWrapper();

        wrapper.setDosenDosen(listDosen);
        // larik List diubah menjadi string dengan format XML
        String xml = xstream.toXML(wrapper);

        FileOutputStream fileDosen = null;
        try {
            // membuat nama file & folder tempat menyimpan jika perlu
            fileDosen = new FileOutputStream("fileDosen.xml");

            // mengubah karakter penyusun string xml sebagai 
            // bytes (berbentuk nomor2 kode ASCII
            byte[] bytes = xml.getBytes("UTF-8");

            // menyimpan file dari bytes
            fileDosen.write(bytes);
        } catch (Exception e) {
            System.err.println("Perhatian: " + e.getMessage());
        } finally {
            if (fileDosen != null) {
                try {
                    fileDosen.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
