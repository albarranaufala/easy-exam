/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class Soal {
    private String pertanyaan;
    private StringProperty pertanyaanProperty;
    private StringProperty kebenaranProperty;
    private int kunciJawaban;
    private String pilA;
    private String pilB;
    private String pilC;
    private String pilD;

    public Soal(){
        pertanyaan = "";
        pertanyaanProperty = new SimpleStringProperty("");
        kebenaranProperty = new SimpleStringProperty("");
        kunciJawaban = 0;
        pilA = "";
        pilB = "";
        pilC = "";
        pilD = "";
    }
    
    public StringProperty pertanyaanProperty(){
        this.pertanyaanProperty.set(this.pertanyaan);
        return pertanyaanProperty;
    }
    
    public String getPertanyaan() {
        return pertanyaan;
    }

    public int getKunciJawaban() {
        return kunciJawaban;
    }

    public String getPilA() {
        return pilA;
    }

    public String getPilB() {
        return pilB;
    }

    public String getPilC() {
        return pilC;
    }

    public String getPilD() {
        return pilD;
    }

    public void setPertanyaan(String pertanyaan) {
        this.pertanyaan = pertanyaan;
    }

    public void setKunciJawaban(int kunciJawaban) {
        this.kunciJawaban = kunciJawaban;
    }

    public void setPilA(String pilA) {
        this.pilA = pilA;
    }

    public void setPilB(String pilB) {
        this.pilB = pilB;
    }

    public void setPilC(String pilC) {
        this.pilC = pilC;
    }

    public void setPilD(String pilD) {
        this.pilD = pilD;
    }
    
    
}
