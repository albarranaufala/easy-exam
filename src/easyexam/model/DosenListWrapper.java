/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement(name = "dosendosen")
public class DosenListWrapper {

    private List<Dosen> dosendosen;

    @XmlElement(name = "dosen")
    public List<Dosen> getDosenDosen() {
        return this.dosendosen;
    }

    public void setDosenDosen(List<Dosen> dosendosen) {
        this.dosendosen = dosendosen;
    }
}