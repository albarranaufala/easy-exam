/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class Dosen {
    private final StringProperty nama;
    private final StringProperty nid;
    private String password;
    
    public Dosen(String nama, String nid){
        this.nama = new SimpleStringProperty(nama);
        this.nid = new SimpleStringProperty(nid);
        this.password = nid;
    }
    
    public Dosen(){
        this(null,null);
    }
    
    public String getNama(){
        return this.nama.get();
    }
    
    public void setNama(String nama){
        this.nama.set(nama);
    }
    
    public StringProperty namaProperty(){
        return nama;
    }
    
    public String getNID(){
        return this.nid.get();
    }
    
    public void setNID(String nid){
        this.nid.set(nid);
    }
    
    public StringProperty nidProperty(){
        return nid;
    }
    
    public String getPassword(){
        return this.password;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    //Untuk mengkripsi password
    public void encryptPassword(){
        int huruf;
        String kodeBaru = "";
        //perulangan enkripsi
        for(int i = 0; i < this.password.length(); i++){
            huruf = (int) (this.password.charAt(i) + i + 2);
            kodeBaru = kodeBaru  + (char)huruf;
        }
        this.password = kodeBaru;
    }
    
    //Untuk mendekripsi password
    public void decryptPassword(){
        int huruf;
        String kodeBaru = "";
        //perulangan dekripsi
        for(int i = 0; i < this.password.length(); i++){
            huruf = (int)this.password.charAt(i) - i - 2;
            kodeBaru = kodeBaru  + (char)huruf;
        }
        this.password = kodeBaru;
    }
}
