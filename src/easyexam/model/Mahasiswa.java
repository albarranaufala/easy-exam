/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import java.util.ArrayList;
import java.util.List;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author User
 */
public class Mahasiswa {
    private final StringProperty nama;
    private final StringProperty nim;
    private String password;
    private ObservableList<UjianSelesai> ujianUjianSelesai;
            
    public Mahasiswa(){
        this(null,null);
    }
    
    public Mahasiswa(String nama, String nim){
        this.nama = new SimpleStringProperty(nama);
        this.nim = new SimpleStringProperty(nim);
        this.password = nim;
        this.ujianUjianSelesai = FXCollections.observableArrayList();
    }
    
    public String getNama(){
        return nama.get();
    }
    
    public void setNama(String nama){
        this.nama.set(nama);
    }
    
    public StringProperty namaProperty(){
        return this.nama;
    }
    
    public String getNIM(){
        return nim.get();
    }
    
    public void setNIM(String nim){
        this.nim.set(nim);
    }
    
    public StringProperty nimProperty(){
        return this.nim;
    }
    
    public String getPassword(){
        return this.password;
    }
    //Untuk mengkripsi password
    public void encryptPassword(){
        int huruf;
        String kodeBaru = "";
        //perulangan enkripsi
        for(int i = 0; i < this.password.length(); i++){
            huruf = (int) (this.password.charAt(i) + i + 2);
            kodeBaru = kodeBaru  + (char)huruf;
        }
        this.password = kodeBaru;
    }
    
    //Untuk mendekripsi password
    public void decryptPassword(){
        int huruf;
        String kodeBaru = "";
        //perulangan dekripsi
        for(int i = 0; i < this.password.length(); i++){
            huruf = (int)this.password.charAt(i) - i - 2;
            kodeBaru = kodeBaru  + (char)huruf;
        }
        this.password = kodeBaru;
    }
    
    public void setPassword(String password){
        this.password = password;
    }
    
    public void tambahUjianSelesai(){
        ujianUjianSelesai.add(new UjianSelesai());
    }
    
    public List<UjianSelesai> getUjianUjianSelesai(){
        return this.ujianUjianSelesai;
    }
    
    public ObservableList<UjianSelesai> getUjianUjianSelesaiObservable(){
        return this.ujianUjianSelesai;
    }
    
    public int getIndeksUjianSelesai(Ujian ujian){
        int indeks = -1;
        for(int i = 0; i < ujianUjianSelesai.size(); i++){
            if(ujianUjianSelesai.get(i).getKodeUjian().equals(ujian.getKodeUjian())){
                indeks = i;
            }
        }
        return indeks;
    }
    
    public UjianSelesai getUjianSelesaiTerpilih(Ujian ujian){
        int indeks = getIndeksUjianSelesai(ujian);
        if(indeks == -1){
            return null;
        }
        else{
            return ujianUjianSelesai.get(indeks);
        }
    }
    
//    public UjianSelesai getUjianSelesaiTerpilih2(UjianSelesai ujian){
//        int indeks = getIndeksUjianSelesai(ujian);
//        if(indeks == -1){
//            return null;
//        }
//        else{
//            return ujianUjianSelesai.get(indeks);
//        }
//    }
}
