/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author User
 */
public class Ujian {

    private final StringProperty judulUjian;
    private final StringProperty kodeUjian;
    private final StringProperty jumlahSoalStr;
    private final StringProperty ketuntasanStr;
    private int jumlahSoal;
    private int nilaiKetuntasan;
    private IntegerProperty nomorProperty;
    private List<Soal> soalsoal;
    private ObservableList<Soal> soalsoalObservable;
    private ObservableList<Mahasiswa> listMahasiswaSelesai;

    public Ujian() {
        this(null, null, 0, 0);
    }

    public Ujian(String judulUjian, String kodeUjian, int jumlahSoal, int nilaiKetuntasan) {
        this.judulUjian = new SimpleStringProperty(judulUjian);
        this.kodeUjian = new SimpleStringProperty(kodeUjian);
        this.jumlahSoal = jumlahSoal;
        this.nomorProperty = new SimpleIntegerProperty(0);
        this.nilaiKetuntasan = nilaiKetuntasan;
        this.listMahasiswaSelesai = FXCollections.observableArrayList();
        this.soalsoal = new ArrayList();
        this.jumlahSoalStr = new SimpleStringProperty(Integer.toString(jumlahSoal));
        this.ketuntasanStr = new SimpleStringProperty(Integer.toString(nilaiKetuntasan));
    }

    public ObservableList<Soal> getSoalSoalObservable() {
        this.soalsoalObservable = FXCollections.observableArrayList(this.soalsoal);
        return this.soalsoalObservable;
    }

    public String getJudulUjian() {
        return judulUjian.get();
    }

    public void setJudulUjian(String judulUjian) {
        this.judulUjian.set(judulUjian);
    }

    public StringProperty judulSoalProperty() {
        return this.judulUjian;
    }

    public String getKodeUjian() {
        return kodeUjian.get();
    }

    public StringProperty kodeSoalProperty() {
        return this.kodeUjian;
    }

    public void setKodeUjian(String kodeUjian) {
        this.kodeUjian.set(kodeUjian);
    }

    public int getJumlahSoal() {
        return jumlahSoal;
    }

    public int getNilaiKetuntasan() {
        return nilaiKetuntasan;
    }

    public List<Soal> getSoalsoal() {
        return soalsoal;
    }

    public void setJumlahSoal(int jumlahSoal) {
        this.jumlahSoal = jumlahSoal;
    }

    public StringProperty jumlahSoalProperty() {
        this.jumlahSoalStr.set(Integer.toString(this.getJumlahSoal()));
        return this.jumlahSoalStr;
    }

    public void setNilaiKetuntasan(int nilaiKetuntasan) {
        this.nilaiKetuntasan = nilaiKetuntasan;
    }

    public StringProperty ketuntasanProperty() {
        this.ketuntasanStr.set(Integer.toString(this.getNilaiKetuntasan()));
        return this.ketuntasanStr;
    }

    public void setSoalsoal(List<Soal> soalsoal) {
        this.soalsoal = soalsoal;
    }

    public void tambahSoal() {
        this.soalsoal.add(new Soal());
    }

    public ObservableList<Mahasiswa> getMahasiswaSelesai() {
        return this.listMahasiswaSelesai;
    }

    public void setMahasiswaSelesai(ObservableList<Mahasiswa> listMahasiswaSelesai) {
        this.listMahasiswaSelesai = listMahasiswaSelesai;
    }

    public int getJumlahA() {
        int banyakA = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("A")) {
                banyakA++;
            }
        }
        return banyakA;
    }

    public int getJumlahAmin() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("A-")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahAperB() {
        int banyakAperB = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("A/B")) {
                banyakAperB++;
            }
        }
        return banyakAperB;
    }

    public int getJumlahBplus() {
        int banyakBplus = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("B+")) {
                banyakBplus++;
            }
        }
        return banyakBplus;
    }

    public int getJumlahB() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("B")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahBmin() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("B-")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahBperC() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("B/C")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahCplus() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("C+")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahC() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("C")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahCmin() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("C-")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahCperD() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("C/D")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahDplus() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("D+")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahD() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("D")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getJumlahE() {
        int banyakAmin = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            if (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).getNilaiHurufProperty().get().equals("E")) {
                banyakAmin++;
            }
        }
        return banyakAmin;
    }

    public int getIndeksUjianSelesai(Mahasiswa mahasiswa) {
        int indeks = -1;
        for (int i = 0; i < listMahasiswaSelesai.size(); i++) {
            if (listMahasiswaSelesai.get(i).equals(mahasiswa.getNIM())) {
                indeks = i;
            }
        }
        return indeks;
    }

    public double getMean() {
        double jumlah = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            jumlah = jumlah + this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai();
        }
        double mean = jumlah / this.getMahasiswaSelesai().size();
        return mean;
    }

    public int getModus() {
        int frekuensi = 0;
        int frekuensiTmp;
        int modus = 0;
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            int nilaiTmp = this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai();
            frekuensiTmp = 0;
            for (int j = 0; j < this.getMahasiswaSelesai().size(); j++) {
                if (nilaiTmp == this.getMahasiswaSelesai().get(j).getUjianSelesaiTerpilih(this).hitungNilai()) {
                    frekuensiTmp++;
                }
            }
            if (frekuensiTmp >= frekuensi) {
                frekuensi = frekuensiTmp;
                modus = this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai();
            }
        }
        return modus;
    }

    public double getVarians() {
        double varians = 0;
        double x = 0;

        double xBar = this.getMean();
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            x = x + ((this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai() - xBar) * (this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai() - xBar));
        }
        varians = x / this.getMahasiswaSelesai().size();
        return varians;
    }

    public double getStandardDeviasi() {
        double StandardDeviasi = 0;
        StandardDeviasi = Math.sqrt(this.getVarians());
        return StandardDeviasi;
    }

    public double getMedian() {
        double median = 0;
        List<Integer> listNilai = new ArrayList();
        for (int i = 0; i < this.getMahasiswaSelesai().size(); i++) {
            listNilai.add(this.getMahasiswaSelesai().get(i).getUjianSelesaiTerpilih(this).hitungNilai());
        }
        //mengurutkan list dari terkecil ke terbesar
        Collections.sort(listNilai);
        //rumus median atau Q1
        if (listNilai.size() <= 0) {
            median = 0;
            return median;
        } else {
            if (listNilai.size() % 2 == 0) {
                median = (listNilai.get((listNilai.size() / 2) - 1) + listNilai.get((listNilai.size() / 2))) / 2;
            } else {
                median = listNilai.get((listNilai.size() - 1) / 2);
            }
        }

        return median;
    }
}
