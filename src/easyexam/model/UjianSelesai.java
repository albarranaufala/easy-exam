/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author User
 */
public class UjianSelesai extends Ujian{
    private int[] jawabanMhs;
    private int nilai;
    private StringProperty nilaiStr;
    private StringProperty nilaiHuruf;
    
    public UjianSelesai(){
        super();
        
    }
    
    public void setUjian(Ujian ujian){
        super.setJudulUjian(ujian.getJudulUjian());
        super.setJumlahSoal(ujian.getJumlahSoal());
        super.setKodeUjian(ujian.getKodeUjian());
        super.setNilaiKetuntasan(ujian.getNilaiKetuntasan());
        super.setSoalsoal(ujian.getSoalsoal());
        jawabanMhs = new int[ujian.getJumlahSoal()];
        nilai = 0;
        nilaiStr = new SimpleStringProperty("");
        nilaiHuruf = new SimpleStringProperty("");
    }
    
    public void setJawabanMhs(int indeks, int jawaban){
        jawabanMhs[indeks] = jawaban;
    }

    public int[] getJawabanMhs() {
        return jawabanMhs;
    }
    
    public int getJawabanBenar(){
        int benar = 0;
        for(int i = 0; i < this.getJawabanMhs().length; i++){

            if(this.getSoalsoal().get(i).getKunciJawaban()==this.getJawabanMhs()[i]){
                benar++;
            }
        }
        return benar;
    }
    
    public int getJawabanSalah(){
        int jawabanSalah = this.getJumlahSoal()-this.getJawabanBenar();
        return jawabanSalah;
    }
    
    public int hitungNilai(){
        
        int nilai = this.getJawabanBenar()*100/this.getJumlahSoal();
        this.nilai = nilai;
        return this.nilai;
    }
    
    public StringProperty nilaiProperty(){
        int benar = 0;
        for(int i = 0; i < this.getJawabanMhs().length; i++){

            if(this.getSoalsoal().get(i).getKunciJawaban()==this.getJawabanMhs()[i]){
                benar++;
            }
        }
        int nilai = benar*100/this.getJumlahSoal();
        
        nilaiStr.set(Integer.toString(nilai));
        return nilaiStr;
    }
    
    public StringProperty getNilaiHurufProperty(){
        int nilaiTmp = hitungNilai();
        if(nilaiTmp<0){
            //tidak melakukan apa apa
            this.nilaiHuruf.set("");
            return this.nilaiHuruf;
        }
        else if(nilaiTmp<=49){
            this.nilaiHuruf.set("E");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=52){
            this.nilaiHuruf.set("D");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=54){
            this.nilaiHuruf.set("D+");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=57){
            this.nilaiHuruf.set("C/D");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=59){
            this.nilaiHuruf.set("C-");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=62){
            this.nilaiHuruf.set("C");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=64){
            this.nilaiHuruf.set("C+");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=67){
            this.nilaiHuruf.set("B/C");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=69){
            this.nilaiHuruf.set("B-");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=72){
            this.nilaiHuruf.set("B");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=74){
            this.nilaiHuruf.set("B+");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=77){
            this.nilaiHuruf.set("A/B");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=79){
            this.nilaiHuruf.set("A-");
            return this.nilaiHuruf;
        } else if(nilaiTmp<=100){
            this.nilaiHuruf.set("A");
            return this.nilaiHuruf;
        } else{
            this.nilaiHuruf.set("A");
            return this.nilaiHuruf;
        }
    }
}