package easyexam.model;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Helper class to wrap a list of persons. This is used for saving the
 * list of persons to XML.
 * 
 * @author 
 */
@XmlRootElement(name = "ujianujian")
public class UjianListWrapper {

    private List<Ujian> ujianujian;

    @XmlElement(name = "ujian")
    public List<Ujian> getUjianUjian() {
        return this.ujianujian;
    }

    public void setUjianUjian(List<Ujian> ujianujian) {
        this.ujianujian = ujianujian;
    }
}