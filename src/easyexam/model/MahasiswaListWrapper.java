/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package easyexam.model;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author User
 */
@XmlRootElement(name = "mahasiswamahasiswa")
public class MahasiswaListWrapper {

    private List<Mahasiswa> mahasiswamahasiswa;

    @XmlElement(name = "mahasiswa")
    public List<Mahasiswa> getMahasiswaMahasiswa() {
        return this.mahasiswamahasiswa;
    }

    public void setMahasiswaMahasiswa(List<Mahasiswa> mahasiswamahasiswa) {
        this.mahasiswamahasiswa = mahasiswamahasiswa;
    }
}